bcbio (1.2.9-3) UNRELEASED; urgency=medium

  * Team upload.
  * Drop python3-seqcluster from Build-Depends and downgrade from
    Recommends to Suggests
  * Fix lintian-overrides about script-with-language-extension
  * DEP3
  * Do not ship empty manpage
  * Use pytest instead of node
    Closes: #1018312
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 07 Feb 2023 10:58:48 +0100

bcbio (1.2.9-2) unstable; urgency=medium

  * Team upload.
  * Fix watch file
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 12 Oct 2022 09:38:21 +0200

bcbio (1.2.9-1) unstable; urgency=medium

  * Team Upload

  [ Lance Lin ]
  * New upstream version - 1.2.9
  * Standards-Version: 4.6.1
  * Copyright: updated years to cover 2022

 -- Lance Lin <LQi254@protonmail.com>  Wed, 17 Aug 2022 20:16:42 +0700

bcbio (1.2.8-1) UNRELEASED; urgency=medium

  * New upstream version.
  * Standards-Version: 4.5.1 (routine-update)
  * Removed now obsolete patch on abc collections
  * Updated dependencies
  * Point to system version of Rscript - always

 -- Steffen Moeller <moeller@debian.org>  Fri, 07 May 2021 20:29:57 +0200

bcbio (1.2.5-1) unstable; urgency=medium

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Tue, 26 Jan 2021 13:55:23 +0100

bcbio (1.2.4-3) UNELEASED; urgency=medium

  * Compatibility with Python 3.9
  * Rounded-up Debian experience
    - Added runtime/testing build depdendencies
    - Adjusted names of executables in Debian
      + hts-nim-tools
      + cnvkit

 -- Steffen Moeller <moeller@debian.org>  Sat, 14 Nov 2020 22:29:00 +0100

bcbio (1.2.4-2) unstable; urgency=medium

  * Team upload.
  * Avoid calling Git - use Debian version to specify the code version
    Closes: #973804

 -- Andreas Tille <tille@debian.org>  Fri, 06 Nov 2020 11:16:33 +0100

bcbio (1.2.4-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Steffen Moeller <moeller@debian.org>  Sat, 26 Sep 2020 00:56:30 +0200

bcbio (1.2.0-2) unstable; urgency=medium

  * Team upload.
  * Drop tophat from Build-Depends
    Closes: #953642
  * Add salsa-ci file (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 11 Mar 2020 18:26:32 +0100

bcbio (1.2.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Adjusted path of bcbio-doc for doc-base

 -- Steffen Moeller <moeller@debian.org>  Thu, 13 Feb 2020 18:30:04 +0100

bcbio (1.1.9-2) unstable; urgency=medium

  * Team upload.
  * Drop tophat.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 14 Feb 2020 17:23:23 +0100

bcbio (1.1.9-1) unstable; urgency=medium

  * New upstream version.
  * Updated d/watch

 -- Steffen Moeller <moeller@debian.org>  Wed, 11 Dec 2019 11:39:46 +0100

bcbio (1.1.8-1) unstable; urgency=medium

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Tue, 26 Nov 2019 14:05:42 +0100

bcbio (1.1.7-1) unstable; urgency=medium

  * New upstream version.

  * Added directions to contrib to -doc and python- packages

 -- Steffen Moeller <moeller@debian.org>  Sat, 19 Oct 2019 14:02:34 +0200

bcbio (1.1.5-1) unstable; urgency=medium

  * New upstream version.

  * Better clean after build
  * Tests still fail.

 -- Steffen Moeller <moeller@debian.org>  Fri, 24 May 2019 18:42:30 +0200

bcbio (1.1.4-2) UNRELEASED; urgency=medium

  * Team upload.
  * Recommends: umis

 -- Andreas Tille <tille@debian.org>  Thu, 16 May 2019 09:49:26 +0200

bcbio (1.1.4-1) unstable; urgency=medium

  * New upstream release.
    - Move to Python 3.6. A python2 environment in the install runs
      non python3 compatible programs. The codebase is still compatible
      with python 2.7 but will only get run and tested on python 3 for
      future releases.
    - RNA-seq: fix for race condition when creating the pizzly cache
    - RNA-seq: Add Salmon to multiqc report.
    - RNA-seq single-cell/DGE: Properly strip transcript versions from
      GENCODE GTFs.
    - RNA-seq: Faster and more flexible rRNA biotype lookup.
    - Move to R3.5.1, including updates to all CRAN and Bioconductor
      packages.
    - tumor-only germline prioritization: provide more useful germline
      filtering based on prioritization INFO tag (EPR) rather than filter
      field.
    - Install: do not require fabric for tool and data installs, making
      full codebase compatible with python 3.
    - variant: Filter out variants with missing ALT alleles output
      by GATK4.
    - GATK: enable specification of spark specific parameters with
      `gatk-spark` resources.
    - RNA-seq single-cell/DGE: added `demultiplexed` option. If set to
      True, treat the data as if it has already been demultiplexed into
      cells/wells.
    - Multiple orders of magnitude faster templating with thousands of
      input files.

  * Moved stringtie from suggested to recommended
  * Added dependency on xonsh for testing

 -- Steffen Moeller <moeller@debian.org>  Thu, 11 Apr 2019 12:56:58 +0200

bcbio (1.1.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * bcbio-doc: mark as "Multi-Arch: foreign"
  * debian/patches/python3.patch: removed, applied upstream
  * debian/control: bcbio-doc: section is "doc"
                               and add ${sphinxdoc:Built-Using}
                    python3-bcbio: section is "python"
  * debian/patches/spelling: fix a few spelling errors
  * debian/patches/include_tests

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 10 Feb 2019 00:42:06 -0800

bcbio (1.1.2-3) unstable; urgency=medium

  * Demoted python3-seqcluster to become a recommendation
  * Packaged documentation, which also contributed a man page

 -- Steffen Moeller <moeller@debian.org>  Wed, 23 Jan 2019 15:04:57 +0100

bcbio (1.1.2-2) UNRELEASED; urgency=medium

  * Found Python3 incompatbility. Upstream has it already.

 -- Steffen Moeller <moeller@debian.org>  Mon, 21 Jan 2019 23:56:26 +0100

bcbio (1.1.2-1) UNRELEASED; urgency=medium

  * Initial release (Closes: #903386)

    TODO: Documentation not built

 -- Steffen Moeller <moeller@debian.org>  Thu, 17 Jan 2019 16:37:37 +0100
