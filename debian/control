Source: bcbio
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: contrib/science
Testsuite: autopkgtest-pkg-python
XS-Autobuild: no
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-gffutils,
               python3-mock,
               python3-pandas,
               python3-pybedtools,
               python3-pysam,
               python3-toolz,
               python3-tornado,
               python3-yaml,
               python3-scipy,
               python3-psutil,
               python3-joblib,
# for testing
               atropos <!nocheck>,
               bcftools <!nocheck>,
               bedtools <!nocheck>,
               biobambam2 <!nocheck>,
               bowtie2 <!nocheck>,
               bwa <!nocheck>,
               cnvkit <!nocheck>,
               fastqc <!nocheck>,
               freebayes <!nocheck>,
               gffread <!nocheck>,
               grabix <!nocheck>,
               gsort <!nocheck>,
               hisat2 <!nocheck>,
               hts-nim-tools <!nocheck>,
               libfreetype6 <!nocheck>,
               lumpy-sv <!nocheck>,
               mosdepth <!nocheck>,
               multiqc <!nocheck>,
               perl <!nocheck>,
               libpicard-java <!nocheck>,
               libvcflib-tools <!nocheck>,
               picard-tools <!nocheck>,
               python3-biopython <!nocheck>,
               python3-cyvcf2 <!nocheck>,
               python3-geneimpacts <!nocheck>,
               python3-logbook <!nocheck>,
               python3-requests <!nocheck>,
               python3-pytest <!nocheck>,
               python3-pytest-mock <!nocheck>,
#               python3-seqcluster <!nocheck>,
               pythonpy <!nocheck>,
               rapmap <!nocheck>,
               rna-star <!nocheck>,
               r-base-core <!nocheck>,
               r-bioc-htsfilter <!nocheck>,
               r-bioc-degreport <!nocheck>,
               r-bioc-purecn <!nocheck>,
               r-bioc-summarizedexperiment <!nocheck>,
               r-cran-tidyverse <!nocheck>,
               r-bioc-titancna <!nocheck>,
               r-bioc-tximport <!nocheck>,
               r-other-wasabi <!nocheck>,
               salmon <!nocheck>,
               samblaster <!nocheck>,
               samtools <!nocheck>,
               seqan-apps <!nocheck>,
               seqtk <!nocheck>,
               snpeff <!nocheck>,
               subread <!nocheck>,
               tabix <!nocheck>,
               tophat-recondition <!nocheck>,
               umis <!nocheck>,
               vcfanno <!nocheck>,
               vt <!nocheck>,
               wham-align <!nocheck>,
               xonsh <!nocheck>,
               libhts-dev <!nocheck>,
# documentation
               python3-sphinx
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/bcbio
Vcs-Git: https://salsa.debian.org/med-team/bcbio.git
Homepage: https://github.com/bcbio/bcbio-nextgen
Rules-Requires-Root: no

Package: python3-bcbio
Architecture: all
Section: contrib/python
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-biopython,
         python3-cyvcf2,
         python3-joblib,
         python3-logbook,
         python3-matplotlib,
         python3-psutil,
         python3-pysam,
         python3-requests,
         python3-scipy,
         python3-six,
         python3-tornado
Recommends: cnvkit,
            gsort,
            libfreetype6,
            libhts-dev,
            lumpy-sv,
            mosdepth,
            multiqc,
            python3-arrow,
            python3-geneimpacts,
            python3-h5py,
#            python3-seqcluster,
            python3-statsmodels,
            python3-tabulate,
            r-other-wasabi,
            r-bioc-purecn,
            r-bioc-titancna,
            seqan-apps,
            snpeff,
            vcfanno,
            vt
Suggests: python3-bioblend,
          python3-dnapilib,
          python3-msgpack,
          python3-seqcluster
Description: library for analysing high-throughput sequencing data
 This package installs the Python 3 libraries of the bcbio-nextgen
 toolkit implementing best-practice pipelines for fully automated high
 throughput sequencing analysis.
 .
 A high-level configuration file specifies inputs and analysis parameters
 to drive a parallel pipeline that handles distributed execution,
 idempotent processing restarts and safe transactional steps.  The project
 contributes a shared community resource that handles the data processing
 component of sequencing analysis, providing researchers with more time
 to focus on the downstream biology.

Package: bcbio
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-bcbio
Recommends: bcftools,
            bwa,
            cnvkit,
            cufflinks,
            delly,
            fastqc,
            freebayes,
            grabix,
            libvcflib-tools,
            macs,
            pythonpy,
            rna-star,
            hisat2,
            sambamba,
            samblaster,
            samtools,
            salmon,
            stringtie,
            subread,
            tabix,
            umis,
            varscan,
            wget,
            wham-align
Suggests: toil,
          cwltool,
          kallisto,
# still missing lumpy-sv,
# still missing manta,
# non-free, install manually: novoalign,
          qualimap,
# for cancer heterogeneity analysis with BubbleTree
          libglu1-mesa,
# for low mem instead of rna-star
          tophat2,
# found in source code but not exactly sure about its importance
          tophat-recondition,
          r-bioc-summarizedexperiment,
          r-cran-tidyverse,
          r-other-wasabi
Description: toolkit for analysing high-throughput sequencing data
 This package installs the command line tools of the bcbio-nextgen
 toolkit implementing best-practice pipelines for fully automated high
 throughput sequencing analysis.
 .
 A high-level configuration file specifies inputs and analysis parameters
 to drive a parallel pipeline that handles distributed execution,
 idempotent processing restarts and safe transactional steps.  The project
 contributes a shared community resource that handles the data processing
 component of sequencing analysis, providing researchers with more time
 to focus on the downstream biology.
 .
 This package builds and having it in Debian unstable helps the Debian
 developers to synchronize their efforts. But unless a series of external
 dependencies are not installed manually, the functionality of bcbio in
 Debian is only a shadow of itself. Please use the official distribution
 of bcbio for the time being, which means "use conda". The TODO file in
 the Debian directory should give an overview on progress for Debian
 packaging.
